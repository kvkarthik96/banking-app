# Banking App README

This is a Flutter app that demonstrates a login screen and various functionalities such as navigation, GraphQL API integration, and UI components.

## Login Screen

The app starts with a login screen where users can enter their username and password. The password field is toggleable for showing/hiding the password. After successful login, users are directed to the 'Main screen'.

## Main Screen

The 'Main screen' features a bottom navigation bar with three items: 'Home', 'Accounts', and 'Services'. Each item corresponds to a specific screen.

- Home Screen: Displays the logged-in user's username and uses the Home GraphQL API to show relevant data in UI using the Card widget.

- Accounts Screen: Lists the user's accounts using the Card widget. Clicking on an account takes the user to the 'Transactions screen'.

- Services Screen: Lists various services such as 'Loans', 'Statements', and 'Contacts'.

## Home Screen

The home screen displays the logged-in user's username and fetches data using the Home GraphQL API. The data is presented using the Card widget.

## Accounts Screen

The accounts screen lists the user's accounts using the Card widget. Clicking on an account navigates to the 'Transactions screen'. The Transactions screen displays account details at the top using the Card widget and provides two tabs: 'Transactions' and 'Details' using the TabBar widget.

- Transactions Tab: Displays a list of transactions.

- Details Tab: Displays all account details using the Card widget.

## Services Screen

The services screen lists services such as 'Loans', 'Statements', and 'Contacts'.

- Loans Screen: Shows an error message using a SnackBar when clicked.

- Statements Screen: Displays a dropdown with years for filtering statements using the Statements GraphQL API. Clicking on a statement opens a new screen with a dummy PDF file displayed.

- Contacts Screen: Handles failed GraphQL API response and displays appropriate messages.

## Getting Started

1. Clone this repository.
2. Install dependencies using `flutter pub get`.
3. Run the app using `flutter run`.

## Notes

- Icons: Appropriate assets are used as icons for bottom navigation items.
- GraphQL API: GraphQL APIs from the provided Postman collection are used to fetch and display data.
- UI Components: The Card widget is used to display data in a visually appealing manner.
- PDF Display: A PDF Flutter library is used to display dummy PDF files.

Feel free to explore the codebase for detailed implementations of the described features.

