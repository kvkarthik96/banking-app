class RouteConst {
  static const String routeDefalut = '/';
  static const String routeLoginScreen = '/login';
  static const String routeMainScreen = '/main';
  static const String routeHomeScreen = '/home';
  static const String routeServiceScreen = '/service';
  static const String routeAccountScreen = '/accounts';
  static const String routeStatementsScreen = '/statements';
  static const String routeTransactionsScreen = '/transactions';
  static const String routeContactsScreen = '/contacts';
  static const String routeStatementViewerScreen = '/statementViewerScreen';
}
