import 'package:flutter/material.dart';

const kPrimaryColor = Colors.blue;
const kWhiteColor = Colors.white;
const kBlackColor = Colors.black;
const kTertiaryColor = Color(0xFF808080);