class ImageConst {
  static const contactsImage = "assets/images/contacts.png";
  static const accountsImage = "assets/images/accounts.png";
  static const homeImage = "assets/images/home.png";
  static const loanImage = "assets/images/loan.png";
  static const logoutImage = "assets/images/logout.png";
  static const serviceImage = "assets/images/service.png";
  static const statementsImage = "assets/images/statments.png";
  static const filterImage = "assets/images/filter.png";
}
