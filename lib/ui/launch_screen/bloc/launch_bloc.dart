import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'launch_event.dart';
part 'launch_state.dart';

class LaunchBloc extends Bloc<LaunchEvent, LaunchState> {
  LaunchBloc() : super(LaunchInitial()) {
    on<SessionHandlingEvent>(sessionHandlingEvent);
  }

  Future<FutureOr<void>> sessionHandlingEvent(
      SessionHandlingEvent event, Emitter<LaunchState> emit) async {
    final Future<SharedPreferences> preference =
        SharedPreferences.getInstance();
    final SharedPreferences prefs = await preference;

    bool isLoggedIn = prefs.getBool('isLoggedId') ?? false;

    if (isLoggedIn) {
      emit(NavigateToHomeScreenState());
    } else {
      emit(NavigateToLoginScreenState());
    }
  }
}
