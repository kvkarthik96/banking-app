part of 'launch_bloc.dart';

sealed class LaunchState extends Equatable {
  const LaunchState();

  @override
  List<Object> get props => [];
}

abstract class LaunchStateActionState extends LaunchState {}

final class LaunchInitial extends LaunchState {}

final class NavigateToLoginScreenState extends LaunchStateActionState {}

final class NavigateToHomeScreenState extends LaunchStateActionState {}
