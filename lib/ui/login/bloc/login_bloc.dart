import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial()) {
    on<NavigateToHomeEvent>(navigateToHomeEvent);
  }

  Future<FutureOr<void>> navigateToHomeEvent(
      NavigateToHomeEvent event, Emitter<LoginState> emit) async {
    final Future<SharedPreferences> preference =
        SharedPreferences.getInstance();

    final SharedPreferences prefs = await preference;

    prefs.setString('userName', event.userName);
    prefs.setBool('isLoggedId', true);

    emit(NavigateToHomeState());
  }
}
