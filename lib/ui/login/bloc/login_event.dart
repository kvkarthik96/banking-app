part of 'login_bloc.dart';

sealed class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class NavigateToHomeEvent extends LoginEvent {
  final String userName;

  const NavigateToHomeEvent(this.userName);
}
