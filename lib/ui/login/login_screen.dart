import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/res/route_const.dart';
import 'package:user_app/ui/utils/texts/label_text.dart';
import 'package:user_app/ui/utils/texts/text_field.dart';
import 'package:user_app/ui/utils/validator.dart';
import 'package:user_app/ui/widgets/app_exit_alert.dart';

import 'bloc/login_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final userNameKey = GlobalKey<FormFieldState>();
  final passwordKey = GlobalKey<FormFieldState>();

  final TextEditingController userNameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  LoginBloc loginBloc = LoginBloc();

  bool obscureText = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(
      bloc: loginBloc,
      // listenWhen: (previous, current) => current == NavigateToHomeState,
      listener: (context, state) {
        if (state is NavigateToHomeState) {
          context.go(RouteConst.routeMainScreen);
        }
      },
      builder: (context, state) => WillPopScope(
        onWillPop: () async {
          showAppExitAlert(context);
          return false;
        },
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Scaffold(
              body: SingleChildScrollView(
                  child: Container(
            height: 1.sh,
            color: kWhiteColor,
            padding: EdgeInsets.symmetric(horizontal: 25.w),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 80.h),
                const LabelText(
                    text: "Login",
                    color: kBlackColor,
                    size: 24,
                    fontWeight: FontWeight.bold),
                const SizedBox(height: 30),
                CustomTextField(
                    formKey: userNameKey,
                    controller: userNameController,
                    hintText: "User Name *",
                    keyboardType: TextInputType.emailAddress,
                    validator: validateName,
                    onChanged: (text) {
                      userNameKey.currentState!.validate();
                    }),
                const SizedBox(height: 25),
                CustomTextField(
                    formKey: passwordKey,
                    controller: passwordController,
                    hintText: "Password *",
                    keyboardType: TextInputType.text,
                    obscureText: obscureText,
                    validator: validatePassword,
                    suffixIcon: IconButton(
                      onPressed: () {
                        obscureText = !obscureText;
                        setState(() {});
                      },
                      icon: Icon(
                          obscureText ? Icons.visibility_off : Icons.visibility,
                          color: Colors.grey),
                    ),
                    onChanged: (text) {
                      passwordKey.currentState!.validate();
                    }),
                const SizedBox(height: 50),
                SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              vertical: 8, horizontal: 40),
                          backgroundColor: kPrimaryColor),
                      onPressed: () {
                        if (userNameKey.currentState!.validate() &&
                            passwordKey.currentState!.validate()) {
                          loginBloc.add(
                              NavigateToHomeEvent(userNameController.text));
                        }
                      },
                      child: const LabelText(
                          text: "Login",
                          color: kWhiteColor,
                          size: 16,
                          fontWeight: FontWeight.w600)),
                )
              ],
            ),
          ))),
        ),
      ),
    );
  }
}
