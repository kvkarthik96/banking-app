import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_app/data/remote/service.dart';
import 'package:user_app/ui/utils/helper_util.dart';

part 'contacts_event.dart';
part 'contacts_state.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  ContactsBloc() : super(ContactsLoadingState()) {
    on<GetContactsEvent>(getContactsEvent);
  }

  Future<FutureOr<void>> getContactsEvent(
      GetContactsEvent event, Emitter<ContactsState> emit) async {
    Service service = Service();

    dynamic response;

    try {
      getContactsList() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await service.getContactsListAPI().then((respObj) {
              response = respObj;
            });
          }
        });
      }

      emit(ContactsLoadingState());
      await getContactsList();

      response != null
          ? emit(ContactsLoadedState(response))
          : emit(ContactsErrorState());
    } catch (e) {
      emit(ContactsErrorState());
    }
  }
}
