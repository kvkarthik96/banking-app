part of 'contacts_bloc.dart';

sealed class ContactsState extends Equatable {
  const ContactsState();

  @override
  List<Object> get props => [];
}

final class ContactsInitialState extends ContactsState {}

final class ContactsLoadingState extends ContactsState {}

final class ContactsLoadedState extends ContactsState {
  final dynamic response;

  const ContactsLoadedState(this.response);
}

final class ContactsErrorState extends ContactsState {}
