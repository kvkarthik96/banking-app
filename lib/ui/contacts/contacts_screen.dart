import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/ui/contacts/bloc/contacts_bloc.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';
import 'package:user_app/ui/widgets/error_widget.dart';
import 'package:user_app/ui/widgets/shimmer_loading_widget.dart';

class ContactsScreen extends StatefulWidget {
  const ContactsScreen({super.key});

  @override
  State<ContactsScreen> createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
  ContactsBloc contactsBloc = ContactsBloc();

  @override
  void initState() {
    contactsBloc.add(GetContactsEvent());
    super.initState();
  }

  @override
  void dispose() {
    contactsBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactsBloc, ContactsState>(
      bloc: contactsBloc,
      builder: (context, state) {
        if (state is ContactsLoadingState) {
          return Scaffold(
            appBar: AppWidgets.appBarWidget(
                isBackButton: true,
                title: 'Contacts',
                onPressed: () {
                  context.pop();
                }),
            body: const ShimmerLoadingWidget(),
          );
        } else if (state is ContactsLoadedState) {
          return Scaffold(
            appBar: AppWidgets.appBarWidget(
                isBackButton: true,
                title: 'Contacts',
                onPressed: () {
                  context.pop();
                }),
          );
        } else {
          return Scaffold(
            appBar: AppWidgets.appBarWidget(
                isBackButton: true,
                title: 'Contacts',
                onPressed: () {
                  context.pop();
                }),
            body: const ShowErrorWidget(),
          );
        }
      },
    );
  }
}
