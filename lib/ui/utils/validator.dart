String? validateName(String? value) {
  value = value!.replaceAll(RegExp(' +'), ' ').trim();
  if (value.isEmpty) {
    return "Please enter your  name";
  } else if (value.length <= 2) {
    return "Name must be more than 2 characters";
  } else {
    return null;
  }
}

String? validatePassword(String? value) {
  value = value!.replaceAll(RegExp(' +'), ' ').trim();
  String pattern =
      r'^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$';
  RegExp passwordRegex = RegExp(pattern);

  if (value.isEmpty) {
    return "Please enter password";
  } else if (value.length < 6 || !passwordRegex.hasMatch(value)) {
    return "Minimum six characters, at least one letter, one number and one special character";
  } else {
    return null;
  }
}
