import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_app/data/model/home_model.dart';
import 'package:user_app/data/remote/service.dart';
import 'package:user_app/ui/utils/helper_util.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeLoadingState()) {
    on<GetHomeDetailsEvent>(getHomeDetailsEvent);
  }

  Future<FutureOr<void>> getHomeDetailsEvent(
      GetHomeDetailsEvent event, Emitter<HomeState> emit) async {
    HomeModel response = HomeModel();
    Service service = Service();

    final Future<SharedPreferences> preference =
        SharedPreferences.getInstance();
    final SharedPreferences prefs = await preference;

    String userName = prefs.getString('userName') ?? "";

    getHomeDeatils() async {
      await HelperUtil.checkInternetConnection().then((internet) async {
        if (internet) {
          await service.getHomeDetailsAPI().then((respObj) {
            response = respObj;
          });
        }
      });
    }

    emit(HomeLoadingState());
    await getHomeDeatils();
    emit(HomeLoadedState(response, userName));
  }
}
