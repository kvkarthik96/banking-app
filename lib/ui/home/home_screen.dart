import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:user_app/data/model/home_model.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/ui/home/bloc/home_bloc.dart';
import 'package:user_app/ui/utils/texts/label_text.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';
import 'package:user_app/ui/widgets/description_widget.dart';
import 'package:user_app/ui/widgets/error_widget.dart';
import 'package:user_app/ui/widgets/shimmer_loading_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeBloc homeBloc = HomeBloc();

  List<dynamic> characters = [];

  @override
  void initState() {
    homeBloc.add(GetHomeDetailsEvent());

    super.initState();
  }

  @override
  void dispose() {
    homeBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      bloc: homeBloc,
      builder: (context, state) {
        if (state is HomeLoadingState) {
          return const Scaffold(
            body: ShimmerLoadingWidget(),
          );
        } else if (state is HomeLoadedState) {
          HomeData? homeData = state.response.home;
          return Scaffold(
            body: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.w),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Card(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 12.w, vertical: 15.h),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(width: double.infinity),
                            DescriptionWidget(
                                title: 'Name', desc: state.userName),
                            DescriptionWidget(
                                title: 'Account Number',
                                desc: homeData?.accountNumber ?? ''),
                            DescriptionWidget(
                                title: 'Balance',
                                desc:
                                    '${homeData?.balance?.toStringAsFixed(2)}'),
                            DescriptionWidget(
                                title: 'Currency',
                                desc: homeData?.currency ?? ''),
                            DescriptionWidget(
                                title: 'Address',
                                desc:
                                    '#${homeData?.address?.buildingNumber}, ${homeData?.address?.streetName}\n${homeData?.address?.townName}, ${homeData?.address?.country}\n${homeData?.address?.postCode}'),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 15.h),
                    Theme(
                        data: ThemeData()
                            .copyWith(dividerColor: Colors.transparent),
                        child: ListTileTheme(
                            contentPadding: const EdgeInsets.only(right: 10),
                            child: ExpansionTile(
                                iconColor: kBlackColor,
                                initiallyExpanded: true,
                                collapsedIconColor: kBlackColor,
                                expandedAlignment: Alignment.topLeft,
                                title: const LabelText(
                                    text: "Recent Transactions",
                                    color: kPrimaryColor,
                                    size: 16,
                                    fontWeight: FontWeight.bold),
                                children: <Widget>[
                                  homeData != null &&
                                          homeData
                                              .recentTransactions!.isNotEmpty
                                      ? ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: homeData
                                              .recentTransactions!.length,
                                          itemBuilder: (context, index) {
                                            RecentTransactions transactions =
                                                homeData
                                                    .recentTransactions![index];

                                            return Card(
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.w,
                                                    vertical: 15.h),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const SizedBox(
                                                        width: double.infinity),
                                                    DescriptionWidget(
                                                        title: 'Date',
                                                        desc:
                                                            transactions.date!),
                                                    DescriptionWidget(
                                                        title: 'Description',
                                                        desc: transactions
                                                            .description!),
                                                    DescriptionWidget(
                                                        title: 'Amount',
                                                        desc: transactions
                                                            .amount!
                                                            .toStringAsFixed(
                                                                2)),
                                                    DescriptionWidget(
                                                        title: 'From Account',
                                                        desc: transactions
                                                            .fromAccount!),
                                                    DescriptionWidget(
                                                        title: 'To Account',
                                                        desc: transactions
                                                            .toAccount!),
                                                  ],
                                                ),
                                              ),
                                            );
                                          })
                                      : AppWidgets.noDataFoundWidget()
                                ]))),
                    SizedBox(height: 10.h),
                    Theme(
                        data: ThemeData()
                            .copyWith(dividerColor: Colors.transparent),
                        child: ListTileTheme(
                            contentPadding: const EdgeInsets.only(right: 10),
                            child: ExpansionTile(
                                iconColor: kBlackColor,
                                initiallyExpanded: true,
                                collapsedIconColor: kBlackColor,
                                expandedAlignment: Alignment.topLeft,
                                title: const LabelText(
                                    text: "Upcoming Bills",
                                    color: kPrimaryColor,
                                    size: 16,
                                    fontWeight: FontWeight.bold),
                                children: <Widget>[
                                  homeData != null &&
                                          homeData.upcomingBills!.isNotEmpty
                                      ? ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount:
                                              homeData.upcomingBills!.length,
                                          itemBuilder: (context, index) {
                                            UpcomingBills upcomingBills =
                                                homeData.upcomingBills![index];
                                            return Card(
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.w,
                                                    vertical: 15.h),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const SizedBox(
                                                        width: double.infinity),
                                                    DescriptionWidget(
                                                        title: 'Date',
                                                        desc: upcomingBills
                                                            .date!),
                                                    DescriptionWidget(
                                                        title: 'Description',
                                                        desc: upcomingBills
                                                            .description!),
                                                    DescriptionWidget(
                                                        title: 'Amount',
                                                        desc: upcomingBills
                                                            .amount!
                                                            .toStringAsFixed(
                                                                2)),
                                                    DescriptionWidget(
                                                        title: 'From Account',
                                                        desc: upcomingBills
                                                            .fromAccount!),
                                                    DescriptionWidget(
                                                        title: 'To Account',
                                                        desc: upcomingBills
                                                            .toAccount!),
                                                  ],
                                                ),
                                              ),
                                            );
                                          })
                                      : AppWidgets.noDataFoundWidget()
                                ]))),
                  ],
                ),
              ),
            ),
          );
        } else {
          return const Scaffold(
            body: ShowErrorWidget(),
          );
        }
      },
    );
  }
}
