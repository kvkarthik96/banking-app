import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';

class StatementViewerScreen extends StatefulWidget {
  const StatementViewerScreen({super.key});

  @override
  State<StatementViewerScreen> createState() => _StatementViewerScreenState();
}

class _StatementViewerScreenState extends State<StatementViewerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppWidgets.appBarWidget(
          title: 'View Statement',
          onPressed: () {
            context.pop();
          }),
      body: SizedBox(
          height: 1.sh,
          width: 1.sw,
          child: SfPdfViewer.network(
            "https://cdn.syncfusion.com/content/PDFViewer/flutter-succinctly.pdf",
          )),
    );
  }
}
