part of 'accounts_bloc.dart';

sealed class AccountsEvent extends Equatable {
  const AccountsEvent();

  @override
  List<Object> get props => [];
}

class GetAccountsEvent extends AccountsEvent {}

class NavigateToTransactionsEvent extends AccountsEvent{
  final Accounts data;

 const NavigateToTransactionsEvent(this.data);
}
