part of 'accounts_bloc.dart';

sealed class AccountsState extends Equatable {
  const AccountsState();

  @override
  List<Object> get props => [];
}

abstract class AccountsActionState extends AccountsState {}

final class AccountsLoadingState extends AccountsState {}

final class AccountsLoadedState extends AccountsState {
  final AccountsModel response;

  const AccountsLoadedState(this.response);
}

final class AccountsErrorState extends AccountsState {}

final class AccountsActionInitialState extends AccountsActionState {}

final class NavigateToTransactionssState extends AccountsActionState {
  final Accounts data;
  NavigateToTransactionssState(this.data);
}
