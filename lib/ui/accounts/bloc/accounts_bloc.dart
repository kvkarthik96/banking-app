import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_app/data/model/accounts_model.dart';
import 'package:user_app/data/remote/service.dart';
import 'package:user_app/ui/utils/helper_util.dart';

part 'accounts_event.dart';
part 'accounts_state.dart';

class AccountsBloc extends Bloc<AccountsEvent, AccountsState> {
  AccountsBloc() : super(AccountsLoadingState()) {
    on<GetAccountsEvent>(getAccountsEvent);

    on<NavigateToTransactionsEvent>(navigateToTransactionsEvent);
  }

  Future<FutureOr<void>> getAccountsEvent(
      GetAccountsEvent event, Emitter<AccountsState> emit) async {
    AccountsModel response = AccountsModel();
    Service service = Service();

    try {
      getAcccountsList() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await service.getAccountsListAPI().then((respObj) {
              response = respObj;
            });
          }
        });
      }

      emit(AccountsLoadingState());
      await getAcccountsList();
      emit(AccountsLoadedState(response));
    } catch (e) {
      emit(AccountsErrorState());
    }
  }

  FutureOr<void> navigateToTransactionsEvent(
      NavigateToTransactionsEvent event, Emitter<AccountsState> emit) {
    emit(AccountsActionInitialState());
    emit(NavigateToTransactionssState(event.data));
  }
}
