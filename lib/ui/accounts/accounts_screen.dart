import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/data/model/accounts_model.dart';
import 'package:user_app/res/route_const.dart';
import 'package:user_app/ui/accounts/bloc/accounts_bloc.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';
import 'package:user_app/ui/widgets/description_widget.dart';
import 'package:user_app/ui/widgets/error_widget.dart';
import 'package:user_app/ui/widgets/shimmer_loading_widget.dart';

class AccountsScreen extends StatefulWidget {
  const AccountsScreen({super.key});

  @override
  State<AccountsScreen> createState() => _AccountsScreenState();
}

class _AccountsScreenState extends State<AccountsScreen> {
  AccountsBloc accountsBloc = AccountsBloc();

  @override
  void initState() {
    accountsBloc.add(GetAccountsEvent());
    super.initState();
  }

  @override
  void dispose() {
    accountsBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AccountsBloc, AccountsState>(
      bloc: accountsBloc,
      listenWhen: (previous, current) => current is AccountsActionState,
      buildWhen: (previous, current) => current is! AccountsActionState,
      listener: (context, state) {
        if (state is NavigateToTransactionssState) {
          context.push(RouteConst.routeTransactionsScreen, extra: state.data);
        }
      },
      builder: (context, state) {
        if (state is AccountsLoadingState) {
          return const Scaffold(
            body: ShimmerLoadingWidget(),
          );
        } else if (state is AccountsLoadedState) {
          return Scaffold(
              body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: state.response.accounts!.isNotEmpty
                ? ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    itemCount: state.response.accounts!.length,
                    itemBuilder: (context, index) {
                      Accounts account = state.response.accounts![index];
                      return InkWell(
                        onTap: () {
                          accountsBloc
                              .add(NavigateToTransactionsEvent(account));
                        },
                        child: Card(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 12.w, vertical: 15.h),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(width: double.infinity),
                                DescriptionWidget(
                                    title: 'Account Holder',
                                    desc: account.accountHolder!),
                                DescriptionWidget(
                                    title: 'Account Number',
                                    desc: account.accountNumber!),
                                DescriptionWidget(
                                    title: 'Account Type',
                                    desc: account.accountType!),
                                DescriptionWidget(
                                    title: 'Balance',
                                    desc: account.balance!.toStringAsFixed(2)),
                              ],
                            ),
                          ),
                        ),
                      );
                    })
                : AppWidgets.noDataFoundWidget(),
          ));
        } else {
          return const Scaffold(
            body: ShowErrorWidget(),
          );
        }
      },
    );
  }
}
