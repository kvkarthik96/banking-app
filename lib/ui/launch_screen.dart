import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/res/route_const.dart';
import 'package:user_app/ui/utils/texts/label_text.dart';

class LaunchScreen extends StatefulWidget {
  const LaunchScreen({super.key});

  @override
  State<LaunchScreen> createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 3)).then((_) {
      navigateTo();
    });
    super.initState();
  }

  navigateTo() async {
    final Future<SharedPreferences> preference =
        SharedPreferences.getInstance();
    final SharedPreferences prefs = await preference;
    bool isLoggedIn = prefs.getBool('isLoggedId') ?? false;

    if (isLoggedIn) {
      // ignore: use_build_context_synchronously
      context.go(RouteConst.routeMainScreen);
    } else {
      // ignore: use_build_context_synchronously
      context.go(RouteConst.routeLoginScreen);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.blue, Colors.red])),
      child: const Center(
          child: LabelText(
              text: 'Welcome to Banking App',
              fontWeight: FontWeight.bold,
              size: 22,
              color: kWhiteColor)),
    ));
  }
}
