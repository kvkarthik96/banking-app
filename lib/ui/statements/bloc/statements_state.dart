part of 'statements_bloc.dart';

sealed class StatementsState extends Equatable {
  const StatementsState();

  @override
  List<Object> get props => [];
}

abstract class StatementActionState extends StatementsState {}

final class StatementsInitial extends StatementsState {}

final class StatementsLoadingState extends StatementsState {}

final class StatementsLoadedState extends StatementsState {
  final StatementsModel response;
  final List<Statements> statements;

  const StatementsLoadedState(
      {required this.response, required this.statements});
}

final class StatementsErrorState extends StatementsState {}

final class ActionsIntialState extends StatementActionState {}

final class NavigateToViewPdfState extends StatementActionState {}
