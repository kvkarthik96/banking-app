import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_app/data/model/statements_model.dart';
import 'package:user_app/data/remote/service.dart';
import 'package:user_app/ui/utils/helper_util.dart';

part 'statements_event.dart';
part 'statements_state.dart';

class StatementsBloc extends Bloc<StatementsEvent, StatementsState> {
  StatementsBloc() : super(StatementsInitial()) {
    on<GetStatementsEvent>(getStatementsEvent);

    on<ApplyFilterEvent>(applyFilterEvent);

    on<NavigateToViewPdfEvent>(navigateToViewPdfEvent);
  }

  Future<FutureOr<void>> getStatementsEvent(
      GetStatementsEvent event, Emitter<StatementsState> emit) async {
    StatementsModel response = StatementsModel();
    Service service = Service();

    try {
      getStatementsList() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await service.getStatementsListAPI().then((respObj) {
              response = respObj;
            });
          }
        });
      }

      emit(StatementsLoadingState());
      await getStatementsList();
      emit(StatementsLoadedState(
          response: response, statements: response.statements!));
    } catch (e) {
      emit(StatementsErrorState());
    }
  }

  FutureOr<void> navigateToViewPdfEvent(
      NavigateToViewPdfEvent event, Emitter<StatementsState> emit) {
    emit(ActionsIntialState());
    emit(NavigateToViewPdfState());
  }

  FutureOr<void> applyFilterEvent(
      ApplyFilterEvent event, Emitter<StatementsState> emit) {
    emit(StatementsLoadingState());

    List<Statements> statements = [];
    for (int i = 0; i < event.response.statements!.length; i++) {
      String data = event.response.statements![i].date!;
      if (data.toLowerCase().contains(event.selectedFilter.toLowerCase())) {
        statements.add(event.response.statements![i]);
      }
    }

    if (event.selectedFilter == '') {
      emit(StatementsLoadedState(
          response: event.response, statements: event.response.statements!));
    } else {
      emit(StatementsLoadedState(
          response: event.response, statements: statements));
    }
  }
}
