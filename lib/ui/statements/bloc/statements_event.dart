part of 'statements_bloc.dart';

sealed class StatementsEvent extends Equatable {
  const StatementsEvent();

  @override
  List<Object> get props => [];
}

class GetStatementsEvent extends StatementsEvent {}

class ApplyFilterEvent extends StatementsEvent {
  final String selectedFilter;
  final StatementsModel response;

  const ApplyFilterEvent(
      {required this.selectedFilter, required this.response});
}

class NavigateToViewPdfEvent extends StatementsEvent {}
