import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/data/model/statements_model.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/res/image_const.dart';
import 'package:user_app/res/route_const.dart';
import 'package:user_app/ui/statements/bloc/statements_bloc.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';
import 'package:user_app/ui/widgets/description_widget.dart';

import 'widgets/show_filter_bottomsheet.dart';
import 'widgets/statements_error_widget.dart';
import 'widgets/statements_loading_widget.dart';

class StatementsScreen extends StatefulWidget {
  const StatementsScreen({super.key});

  @override
  State<StatementsScreen> createState() => _StatementsScreenState();
}

class _StatementsScreenState extends State<StatementsScreen> {
  StatementsBloc statementsBloc = StatementsBloc();

  String selFilter = '';

  @override
  void initState() {
    statementsBloc.add(GetStatementsEvent());
    super.initState();
  }

  @override
  void dispose() {
    statementsBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<StatementsBloc, StatementsState>(
      bloc: statementsBloc,
      listenWhen: (previous, current) => current is StatementActionState,
      buildWhen: (previous, current) => current is! StatementActionState,
      listener: (context, state) {
        if (state is NavigateToViewPdfState) {
          context.push(RouteConst.routeStatementViewerScreen);
        }
      },
      builder: (context, state) {
        if (state is StatementsLoadingState) {
          return const StatementsLoadingWidget();
        } else if (state is StatementsLoadedState) {
          return Scaffold(
            appBar: AppWidgets.appBarWidget(
                isBackButton: true,
                title: 'Statements',
                onPressed: () {
                  context.pop();
                },
                actions: [
                  IconButton(
                    onPressed: () {
                      showOptionsBottomSheet(
                          context: context,
                          onTap: (String selValue) {
                            selFilter = selValue;
                            Navigator.pop(context);

                            statementsBloc.add(ApplyFilterEvent(
                                response: state.response,
                                selectedFilter: selFilter));
                          },
                          selectedValue: selFilter);
                    },
                    icon: Image.asset(ImageConst.filterImage,
                        height: 20.h,
                        width: 20.h,
                        color: selFilter == '' ? kBlackColor : kPrimaryColor),
                  )
                ]),
            body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: state.statements.isNotEmpty
                  ? ListView.builder(
                      physics: const ClampingScrollPhysics(),
                      itemCount: state.statements.length,
                      itemBuilder: (context, index) {
                        Statements statements = state.statements[index];
                        return InkWell(
                          onTap: () {
                            statementsBloc.add(NavigateToViewPdfEvent());
                          },
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 12.w, vertical: 15.h),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(width: double.infinity),
                                  DescriptionWidget(
                                      title: 'Date', desc: statements.date!),
                                  DescriptionWidget(
                                      title: 'Amount',
                                      desc: statements.amount!
                                          .toStringAsFixed(2)),
                                  DescriptionWidget(
                                      title: 'Description',
                                      desc: statements.description!),
                                ],
                              ),
                            ),
                          ),
                        );
                      })
                  : AppWidgets.noDataFoundWidget(),
            ),
          );
        } else {
          return const StatementsErrorWidget();
        }
      },
    );
  }
}
