import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';
import 'package:user_app/ui/widgets/shimmer_loading_widget.dart';

class StatementsLoadingWidget extends StatelessWidget {
  const StatementsLoadingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppWidgets.appBarWidget(
          isBackButton: true,
          title: 'Statements',
          onPressed: () {
            context.pop();
          }),
      body: const ShimmerLoadingWidget(),
    );
  }
}
