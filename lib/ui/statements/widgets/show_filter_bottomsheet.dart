import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/ui/utils/texts/label_text.dart';

showOptionsBottomSheet({
  required BuildContext context,
  required Function(String selYear) onTap,
  required String selectedValue,
}) {
  showModalBottomSheet(
    isScrollControlled: true,
    context: context,
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(22.0), topRight: Radius.circular(22.0))),
    builder: (context) {
      return Container(
        width: 1.sw,
        height: 500.h,
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: const EdgeInsets.only(top: 5),
                  width: 80.w,
                  height: 3,
                  decoration: BoxDecoration(
                      color: kBlackColor,
                      borderRadius: BorderRadius.circular(3))),
              Padding(
                  padding: EdgeInsets.only(top: 30.h, bottom: 20.h),
                  child: const LabelText(
                      text: 'SELECT YEAR',
                      color: kPrimaryColor,
                      size: 14,
                      fontWeight: FontWeight.w500)),
              FilterWidget(
                  title: '2019', onTap: onTap, selFilter: selectedValue),
              FilterWidget(
                  title: '2020', onTap: onTap, selFilter: selectedValue),
              FilterWidget(
                  title: '2021', onTap: onTap, selFilter: selectedValue),
              FilterWidget(
                  title: '2022', onTap: onTap, selFilter: selectedValue),
              FilterWidget(
                  title: '2023', onTap: onTap, selFilter: selectedValue),
              const Divider(color: Colors.grey),
              InkWell(
                  onTap: () => onTap(''),
                  child: Padding(
                      padding: EdgeInsets.only(top: 20.h, bottom: 20.h),
                      child: const LabelText(
                          text: "CLEAR ALL FILTER",
                          color: Colors.green,
                          size: 16,
                          fontWeight: FontWeight.bold))),
            ]),
      );
    },
  );
}

class FilterWidget extends StatelessWidget {
  final String title;
  final String selFilter;
  final Function(String selValue) onTap;
  const FilterWidget(
      {super.key,
      required this.title,
      required this.onTap,
      required this.selFilter});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Divider(color: Colors.grey),
        InkWell(
            onTap: () => onTap(title),
            child: Padding(
                padding: EdgeInsets.only(top: 20.h, bottom: 10.h),
                child: LabelText(
                    text: title,
                    color: selFilter == title ? kPrimaryColor : kBlackColor,
                    size: 16,
                    fontWeight: FontWeight.bold))),
      ],
    );
  }
}
