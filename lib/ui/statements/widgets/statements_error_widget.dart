import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';
import 'package:user_app/ui/widgets/error_widget.dart';

class StatementsErrorWidget extends StatelessWidget {
  const StatementsErrorWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppWidgets.appBarWidget(
          isBackButton: true,
          title: 'Statements',
          onPressed: () {
            context.pop();
          }),
      body: const ShowErrorWidget(),
    );
  }
}
