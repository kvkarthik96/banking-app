import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/ui/utils/texts/label_text.dart';

class ServiceCard extends StatelessWidget {
  final String image;
  final String title;
  final Function() onTap;

  const ServiceCard(
      {super.key,
      required this.image,
      required this.title,
      required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 12),
      child: InkWell(
        onTap: onTap,
        child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
                color: kPrimaryColor.withOpacity(0.1),
                borderRadius: BorderRadius.circular(15)),
            height: 100.h,
            width: double.infinity,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(image, height: 60.h, width: 60.h),
                  SizedBox(width: 15.w),
                  LabelText(
                      text: title.toUpperCase(),
                      color: kPrimaryColor,
                      size: 18,
                      fontWeight: FontWeight.bold)
                ])),
      ),
    );
  }
}
