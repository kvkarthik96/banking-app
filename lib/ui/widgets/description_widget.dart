import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/ui/utils/texts/label_text.dart';

class DescriptionWidget extends StatelessWidget {
  final String title;
  final String desc;
  const DescriptionWidget({super.key, required this.title, required this.desc});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 15.h),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 4,
              child: LabelText(
                  text: title,
                  size: 14,
                  fontWeight: FontWeight.w600,
                  color: kTertiaryColor)),
          const Expanded(flex: 1, child: LabelText(text: ':', size: 14)),
          Expanded(
              flex: 6,
              child:
                  LabelText(text: desc, size: 14, fontWeight: FontWeight.w600))
        ],
      ),
    );
  }
}
