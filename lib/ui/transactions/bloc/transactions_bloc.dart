import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_app/data/model/transactions_model.dart';
import 'package:user_app/data/remote/service.dart';
import 'package:user_app/ui/utils/helper_util.dart';

part 'transactions_event.dart';
part 'transactions_state.dart';

class TransactionsBloc extends Bloc<TransactionsEvent, TransactionsState> {
  TransactionsBloc() : super(TransactionsInitialState()) {
    on<GetTransactionsEvent>(getTransactionsEvent);
  }

  Future<FutureOr<void>> getTransactionsEvent(
      GetTransactionsEvent event, Emitter<TransactionsState> emit) async {
    TransactionsModel response = TransactionsModel();
    Service service = Service();

    try {
      getTransactionsList() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await service.getTransactionsListAPI().then((respObj) {
              response = respObj;
            });
          }
        });
      }

      emit(TransactionsLoadingState());
      await getTransactionsList();
      emit(TransactionsLoadedState(response));
    } catch (e) {
      emit(TransactionsErrorState());
    }
  }
}
