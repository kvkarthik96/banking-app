part of 'transactions_bloc.dart';

sealed class TransactionsState extends Equatable {
  const TransactionsState();

  @override
  List<Object> get props => [];
}

final class TransactionsInitialState extends TransactionsState {}

final class TransactionsLoadingState extends TransactionsState {}

final class TransactionsLoadedState extends TransactionsState {
  final TransactionsModel response;

  const TransactionsLoadedState(this.response);
}

final class TransactionsErrorState extends TransactionsState {}
