import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/data/model/accounts_model.dart';
import 'package:user_app/data/model/transactions_model.dart';
import 'package:user_app/res/color_const.dart';
import 'package:user_app/ui/transactions/bloc/transactions_bloc.dart';
import 'package:user_app/ui/widgets/description_widget.dart';
import 'package:user_app/ui/widgets/error_widget.dart';
import 'package:user_app/ui/widgets/shimmer_loading_widget.dart';

import '../widgets/app_widgets.dart';

class TransactionsScreen extends StatefulWidget {
  final Accounts data;
  const TransactionsScreen({
    super.key,
    required this.data,
  });

  @override
  State<TransactionsScreen> createState() => _TransactionsScreenState();
}

class _TransactionsScreenState extends State<TransactionsScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  TransactionsBloc transactionsBloc = TransactionsBloc();
  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    _tabController!.index = 0;
    transactionsBloc.add(GetTransactionsEvent());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransactionsBloc, TransactionsState>(
      bloc: transactionsBloc,
      builder: (context, state) {
        if (state is TransactionsLoadingState) {
          return Scaffold(
            appBar: AppWidgets.appBarWidget(
                isBackButton: true,
                title: 'Transactions',
                onPressed: () {
                  context.pop();
                }),
            body: const ShimmerLoadingWidget(),
          );
        } else if (state is TransactionsLoadedState) {
          return Scaffold(
            appBar: AppWidgets.appBarWidget(
                isBackButton: true,
                title: 'Transactions',
                onPressed: () {
                  context.pop();
                }),
            body: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 12.w, vertical: 15.h),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(width: double.infinity),
                          DescriptionWidget(
                              title: 'Account Holder',
                              desc: widget.data.accountHolder!),
                          DescriptionWidget(
                              title: 'Balance',
                              desc: widget.data.balance!.toStringAsFixed(2)),
                        ],
                      ),
                    ),
                  ),
                ),
                TabBar(
                    controller: _tabController,
                    indicatorColor: kPrimaryColor,
                    indicatorSize: TabBarIndicatorSize.tab,
                    labelColor: kPrimaryColor,
                    labelStyle:
                        TextStyle(fontSize: 14.sp, fontWeight: FontWeight.w500),
                    unselectedLabelColor: kTertiaryColor,
                    tabs: const [
                      Tab(text: 'Transactions'),
                      Tab(text: 'Details')
                    ]),
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      state.response.transactions!.isNotEmpty
                          ? ListView.builder(
                              physics: const ClampingScrollPhysics(),
                              itemCount: state.response.transactions!.length,
                              itemBuilder: (context, index) {
                                Transactions transactions =
                                    state.response.transactions![index];
                                return Card(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 12.w, vertical: 15.h),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(width: double.infinity),
                                        DescriptionWidget(
                                            title: 'Date',
                                            desc: transactions.date!),
                                        DescriptionWidget(
                                            title: 'Amount',
                                            desc: transactions.amount!
                                                .toStringAsFixed(2)),
                                        DescriptionWidget(
                                            title: 'From Account',
                                            desc: transactions.fromAccount!),
                                        DescriptionWidget(
                                            title: 'To Account',
                                            desc: transactions.toAccount!),
                                        DescriptionWidget(
                                            title: 'Description',
                                            desc: transactions.description!),
                                      ],
                                    ),
                                  ),
                                );
                              })
                          : AppWidgets.noDataFoundWidget(),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 12.w, vertical: 15.h),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(width: double.infinity),
                                DescriptionWidget(
                                    title: 'Account Holder',
                                    desc: widget.data.accountHolder!),
                                DescriptionWidget(
                                    title: 'Account Number',
                                    desc: widget.data.accountNumber!),
                                DescriptionWidget(
                                    title: 'Account Type',
                                    desc: widget.data.accountType!),
                                DescriptionWidget(
                                    title: 'Balance',
                                    desc: widget.data.balance!
                                        .toStringAsFixed(2)),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        } else {
          return Scaffold(
            appBar: AppWidgets.appBarWidget(
                isBackButton: true,
                title: 'Transactions',
                onPressed: () {
                  context.pop();
                }),
            body: const ShowErrorWidget(),
          );
        }
      },
    );
  }
}
