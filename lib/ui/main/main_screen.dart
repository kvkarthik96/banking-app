import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_app/res/image_const.dart';
import 'package:user_app/ui/accounts/accounts_screen.dart';
import 'package:user_app/ui/home/home_screen.dart';
import 'package:user_app/ui/main/bloc/main_bloc.dart';
import 'package:user_app/ui/main/custom_navigation.dart';
import 'package:user_app/ui/service/service_screen.dart';
import 'package:user_app/ui/widgets/app_exit_alert.dart';
import 'package:user_app/ui/widgets/app_widgets.dart';

class MainScreen extends StatefulWidget {
  final int? selectedIndex;
  const MainScreen({Key? key, this.selectedIndex = 0}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  MainBloc mainBloc = MainBloc();

  final List<String> itemTitle = ["Home", "Accounts", "Services"];
  final List<String> iconList = [
    ImageConst.homeImage,
    ImageConst.accountsImage,
    ImageConst.serviceImage
  ];

  final tab = const [HomeScreen(), AccountsScreen(), ServiceScreen()];
  bool interrupted = false;

  @override
  void initState() {
    super.initState();

    mainBloc.add(UpdateSelectedTabEvent(widget.selectedIndex ?? 0));
  }

  @override
  void dispose() {
    mainBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainBloc, MainState>(
      bloc: mainBloc,
      builder: (context, state) {
        if (state is MainLoadedState) {
          return WillPopScope(
            onWillPop: () async {
              if (state.selectedTab == 0) {
                showAppExitAlert(context);
              } else {
                mainBloc.add(const UpdateSelectedTabEvent(0));
              }

              return false;
            },
            child: Scaffold(
                appBar: AppWidgets.appBarWidget(
                    isBackButton: false,
                    title: itemTitle[state.selectedTab],
                    onPressed: () {},
                    actions: [
                      IconButton(
                          onPressed: () {
                            showAppExitAlert(context, isLogout: true);
                          },
                          icon: Image.asset(ImageConst.logoutImage))
                    ]),
                bottomNavigationBar: CustomBottomNavigationBar(
                    iconList: iconList,
                    iconTitle: itemTitle,
                    onChange: (val) async {
                      mainBloc.add(UpdateSelectedTabEvent(val));
                    },
                    selectedIndex: state.selectedTab),
                body: tab[state.selectedTab]),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
