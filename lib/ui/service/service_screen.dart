import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/res/image_const.dart';
import 'package:user_app/res/route_const.dart';
import 'package:user_app/ui/service/bloc/services_bloc.dart';
import 'package:user_app/ui/widgets/service_cards.dart';

class ServiceScreen extends StatefulWidget {
  const ServiceScreen({super.key});

  @override
  State<ServiceScreen> createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  ServicesBloc servicesBloc = ServicesBloc();

  @override
  void dispose() {
    servicesBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ServicesBloc, ServicesState>(
      bloc: servicesBloc,
      listener: (context, state) {
        if (state is OnTapLoansState) {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Loans feature coming soon...'),
              duration: Duration(seconds: 3), // Adjust the duration as needed
            ),
          );
        }
        if (state is OnTapStatementsState) {
          context.push(RouteConst.routeStatementsScreen);
        }
        if (state is OnTapContactsState) {
          context.push(RouteConst.routeContactsScreen);
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.w),
            child: Column(
              children: [
                ServiceCard(
                  title: 'Loans',
                  image: ImageConst.loanImage,
                  onTap: () {
                    servicesBloc.add(OnTapLoansEvent());
                  },
                ),
                ServiceCard(
                  title: 'Statements',
                  image: ImageConst.statementsImage,
                  onTap: () {
                    servicesBloc.add(OnTapStatementEvent());
                  },
                ),
                ServiceCard(
                  title: 'Contacts',
                  image: ImageConst.contactsImage,
                  onTap: () {
                    servicesBloc.add(OnTapContactsEvent());
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
