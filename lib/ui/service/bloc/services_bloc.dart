import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'services_event.dart';
part 'services_state.dart';

class ServicesBloc extends Bloc<ServicesEvent, ServicesState> {
  ServicesBloc() : super(ServicesInitialState()) {
    on<OnTapLoansEvent>(onTapLoansEvent);
    on<OnTapStatementEvent>(onTapStatementEvent);
    on<OnTapContactsEvent>(onTapContactsEvent);
  }

  FutureOr<void> onTapLoansEvent(
      OnTapLoansEvent event, Emitter<ServicesState> emit) {
    emit(ServicesInitialState());
    emit(OnTapLoansState());
  }

  FutureOr<void> onTapStatementEvent(
      OnTapStatementEvent event, Emitter<ServicesState> emit) {
    emit(ServicesInitialState());
    emit(OnTapStatementsState());
  }

  FutureOr<void> onTapContactsEvent(
      OnTapContactsEvent event, Emitter<ServicesState> emit) {
    emit(ServicesInitialState());
    emit(OnTapContactsState());
  }
}
