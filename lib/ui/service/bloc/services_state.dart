part of 'services_bloc.dart';

sealed class ServicesState extends Equatable {
  const ServicesState();

  @override
  List<Object> get props => [];
}

final class ServicesInitialState extends ServicesState {}

final class OnTapLoansState extends ServicesState {}

final class OnTapStatementsState extends ServicesState {}

final class OnTapContactsState extends ServicesState {}
