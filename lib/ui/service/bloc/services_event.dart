part of 'services_bloc.dart';

sealed class ServicesEvent extends Equatable {
  const ServicesEvent();

  @override
  List<Object> get props => [];
}

class OnTapLoansEvent extends ServicesEvent {}

class OnTapStatementEvent extends ServicesEvent {}

class OnTapContactsEvent extends ServicesEvent {}
