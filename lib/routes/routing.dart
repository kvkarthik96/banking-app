import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_app/data/model/accounts_model.dart';
import 'package:user_app/res/route_const.dart';
import 'package:user_app/ui/accounts/accounts_screen.dart';
import 'package:user_app/ui/contacts/contacts_screen.dart';
import 'package:user_app/ui/home/home_screen.dart';
import 'package:user_app/ui/launch_screen.dart';
import 'package:user_app/ui/login/login_screen.dart';
import 'package:user_app/ui/main/main_screen.dart';
import 'package:user_app/ui/service/service_screen.dart';
import 'package:user_app/ui/statements/statements_screen.dart';
import 'package:user_app/ui/transactions/transactions_screen.dart';
import 'package:user_app/ui/view_statement/view_statements.dart';

/// The route configuration.
class MyGoRounterClass {
  static final GoRouter router = GoRouter(
    routes: <RouteBase>[
      GoRoute(
        path: RouteConst.routeDefalut,
        builder: (BuildContext context, GoRouterState state) {
          return const LaunchScreen();
        },
      ),
      GoRoute(
          path: RouteConst.routeLoginScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const LoginScreen();
          }),
      GoRoute(
          path: RouteConst.routeMainScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const MainScreen();
          }),
      GoRoute(
          path: RouteConst.routeHomeScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const HomeScreen();
          }),
      GoRoute(
          path: RouteConst.routeServiceScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const ServiceScreen();
          }),
      GoRoute(
          path: RouteConst.routeAccountScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const AccountsScreen();
          }),
      GoRoute(
          path: RouteConst.routeStatementsScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const StatementsScreen();
          }),
      GoRoute(
          path: RouteConst.routeTransactionsScreen,
          builder: (BuildContext context, GoRouterState state) {
            return TransactionsScreen(
              data: state.extra as Accounts,
            );
          }),
      GoRoute(
          path: RouteConst.routeContactsScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const ContactsScreen();
          }),
      GoRoute(
          path: RouteConst.routeStatementViewerScreen,
          builder: (BuildContext context, GoRouterState state) {
            return const StatementViewerScreen();
          }),
    ],
  );
}
