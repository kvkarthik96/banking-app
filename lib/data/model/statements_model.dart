class StatementsModel {
  List<Statements>? statements;

  StatementsModel({this.statements});

  StatementsModel.fromMap(Map<String, dynamic> json) {
    if (json['statements'] != null) {
      statements = <Statements>[];
      json['statements'].forEach((v) {
        statements!.add(Statements.fromMap(v));
      });
    } else {
      statements = <Statements>[];
    }
  }
}

class Statements {
  String? date;
  String? description;
  double? amount;

  Statements({this.date, this.description, this.amount});

  Statements.fromMap(Map<String, dynamic> json) {
    date = json['date'] ?? '';
    description = json['description'] ?? '';
    amount = json['amount'] != null
        ? json['amount'] is int
            ? json['amount'].toDouble()
            : json['amount']
        : 0;
  }
}
