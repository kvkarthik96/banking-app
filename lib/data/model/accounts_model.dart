class AccountsModel {
  List<Accounts>? accounts;

  AccountsModel({this.accounts});

  AccountsModel.fromMap(Map<String, dynamic> json) {
    if (json['accounts'] != null) {
      accounts = <Accounts>[];
      json['accounts'].forEach((v) {
        accounts!.add(Accounts.fromMap(v));
      });
    } else {
      accounts = <Accounts>[];
    }
  }
}

class Accounts {
  String? id;
  String? accountNumber;
  String? accountType;
  double? balance;
  String? accountHolder;

  Accounts(
      {this.id,
      this.accountNumber,
      this.accountType,
      this.balance,
      this.accountHolder});

  Accounts.fromMap(Map<String, dynamic> json) {
    id = json['id'] ?? '';
    accountNumber = json['accountNumber'] ?? '';
    accountType = json['accountType'] ?? '';
    balance = json['balance'] != null
        ? json['balance'] is int
            ? json['balance'].toDouble()
            : json['balance']
        : 0;
    accountHolder = json['accountHolder'] ?? '';
  }
}
