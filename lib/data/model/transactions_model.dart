class TransactionsModel {
  List<Transactions>? transactions;

  TransactionsModel({this.transactions});

  TransactionsModel.fromMap(Map<String, dynamic> json) {
    if (json['transactions'] != null) {
      transactions = <Transactions>[];
      json['transactions'].forEach((v) {
        transactions!.add(Transactions.fromMap(v));
      });
    } else {
      transactions = <Transactions>[];
    }
  }
}

class Transactions {
  String? date;
  String? description;
  double? amount;
  String? fromAccount;
  String? toAccount;

  Transactions(
      {this.date,
      this.description,
      this.amount,
      this.fromAccount,
      this.toAccount});

  Transactions.fromMap(Map<String, dynamic> json) {
    date = json['date'] ?? '';
    description = json['description'] ?? '';
    amount = json['amount'] != null
        ? json['amount'] is int
            ? json['amount'].toDouble()
            : json['amount']
        : 0;
    fromAccount = json['fromAccount'] ?? '';
    toAccount = json['toAccount'] ?? '';
  }
}
