class HomeModel {
  HomeData? home;

  HomeModel({this.home});

  HomeModel.fromMap(Map<String, dynamic> json) {
    home = json['home'] != null ? HomeData.fromMap(json['home']) : null;
  }
}

class HomeData {
  String? name;
  String? accountNumber;
  double? balance;
  String? currency;
  Address? address;
  List<RecentTransactions>? recentTransactions;
  List<UpcomingBills>? upcomingBills;

  HomeData(
      {this.name,
      this.accountNumber,
      this.balance,
      this.currency,
      this.address,
      this.recentTransactions,
      this.upcomingBills});

  HomeData.fromMap(Map<String, dynamic> json) {
    name = json['name'] ?? '';
    accountNumber = json['accountNumber'] ?? '';
    balance = json['balance'] ?? 0;
    currency = json['currency'] ?? '';
    address = json['address'] != null ? Address.fromMap(json['address']) : null;
    if (json['recentTransactions'] != null) {
      recentTransactions = <RecentTransactions>[];
      json['recentTransactions'].forEach((v) {
        recentTransactions!.add(RecentTransactions.fromMap(v));
      });
    } else {
      recentTransactions = <RecentTransactions>[];
    }
    if (json['upcomingBills'] != null) {
      upcomingBills = <UpcomingBills>[];
      json['upcomingBills'].forEach((v) {
        upcomingBills!.add(UpcomingBills.fromMap(v));
      });
    } else {
      upcomingBills = <UpcomingBills>[];
    }
  }
}

class Address {
  String? streetName;
  String? buildingNumber;
  String? townName;
  String? postCode;
  String? country;

  Address(
      {this.streetName,
      this.buildingNumber,
      this.townName,
      this.postCode,
      this.country});

  Address.fromMap(Map<String, dynamic> json) {
    streetName = json['streetName'] ?? '';
    buildingNumber = json['buildingNumber'] ?? '';
    townName = json['townName'] ?? '';
    postCode = json['postCode'] ?? '';
    country = json['country'] ?? '';
  }
}

class RecentTransactions {
  String? date;
  String? description;
  double? amount;
  String? fromAccount;
  String? toAccount;

  RecentTransactions(
      {this.date,
      this.description,
      this.amount,
      this.fromAccount,
      this.toAccount});

  RecentTransactions.fromMap(Map<String, dynamic> json) {
    date = json['date'] ?? '';
    description = json['description'] ?? '';
    amount = json['amount'] != null
        ? json['amount'] is int
            ? json['amount'].toDouble()
            : json['amount']
        : 0;
    fromAccount = json['fromAccount'] ?? '';
    toAccount = json['toAccount'] ?? '';
  }
}

class UpcomingBills {
  String? date;
  String? description;
  double? amount;
  String? fromAccount;
  String? toAccount;

  UpcomingBills(
      {this.date,
      this.description,
      this.amount,
      this.fromAccount,
      this.toAccount});

  UpcomingBills.fromMap(Map<String, dynamic> json) {
    date = json['date'];
    description = json['description'];
    amount = json['amount'] is int ? json['amount'].toDouble() : json['amount'];
    fromAccount = json['fromAccount'];
    toAccount = json['toAccount'];
  }
}
