import 'package:graphql_flutter/graphql_flutter.dart';

class Network {
  Future<dynamic> fetchGraphQLData(String query) async {
    HttpLink link = HttpLink("http://localhost:4000/");
    GraphQLClient qlClient = GraphQLClient(link: link, cache: GraphQLCache());
    QueryResult queryResult =
        await qlClient.query(QueryOptions(document: gql(query)));
    return queryResult.data;
  }
}
