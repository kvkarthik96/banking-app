class Query {
  static const homeQuery = """
query {

 home {
    name
    accountNumber
    balance
    currency
    address {
      streetName
      buildingNumber
      townName
      postCode
      country
    }
    recentTransactions {
      date
      description
      amount
      fromAccount
      toAccount
    }
    upcomingBills {
      date
      description
      amount
      fromAccount
      toAccount
    }
  }
}""";

  static const accountsQuery = """
query {
 accounts {
    id
    accountNumber
    accountType
    balance
    accountHolder
  }
}""";

  static const statementsQuery = """
query {
  statements {
    date
    description
    amount
  }
}""";

  static const contactsQuery = """
query {
  contacts 
}""";

  static const transactionsQuery = """
query {
 transactions {
    date
    description
    amount
    fromAccount
    toAccount
  }
}""";
}
