import 'package:user_app/data/model/accounts_model.dart';
import 'package:user_app/data/model/home_model.dart';
import 'package:user_app/data/model/statements_model.dart';
import 'package:user_app/data/model/transactions_model.dart';
import 'package:user_app/data/remote/network.dart';
import 'package:user_app/data/remote/queries.dart';

class Service {
  Network network = Network();

  Future<HomeModel> getHomeDetailsAPI() async {
    return network.fetchGraphQLData(Query.homeQuery).then((dynamic res) {
      return HomeModel.fromMap(res);
    });
  }

  Future<StatementsModel> getStatementsListAPI() async {
    return network.fetchGraphQLData(Query.statementsQuery).then((dynamic res) {
      return StatementsModel.fromMap(res);
    });
  }

  Future<AccountsModel> getAccountsListAPI() async {
    return network.fetchGraphQLData(Query.accountsQuery).then((dynamic res) {
      return AccountsModel.fromMap(res);
    });
  }

  Future<TransactionsModel> getTransactionsListAPI() async {
    return network
        .fetchGraphQLData(Query.transactionsQuery)
        .then((dynamic res) {
      return TransactionsModel.fromMap(res);
    });
  }

  Future<dynamic> getContactsListAPI() async {
    return network.fetchGraphQLData(Query.contactsQuery).then((dynamic res) {
      return res;
    });
  }
}
